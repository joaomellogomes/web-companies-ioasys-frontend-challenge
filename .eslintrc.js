const prettierConfig = require('./prettier.config')
const tsconfigbase = require('./tsconfig.base.json')

/* Read tsconfig.paths.json and generate string RegEx to use in eslint-plugin-import-helpers */
const tsconfigPaths = Object.keys(tsconfigbase.compilerOptions.paths) || []
const tsconfigPathsParsed = tsconfigPaths.map(tsconfigPath => tsconfigPath.slice(0, -1))
const tsConfigPathsImportHelpersGroup = tsconfigPathsParsed.map(path => `/^${path}/`)

module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'eslint:recommended',
    'react-app',
    'react-app/jest',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
    'prettier/@typescript-eslint',
    'prettier/standard',
    'prettier/react'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 12,
    sourceType: 'module'
  },
  plugins: ['react', '@typescript-eslint', 'eslint-plugin-import-helpers'],
  rules: {
    indent: ['error', 2],
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'never'],
    'prettier/prettier': ['error', prettierConfig],
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    'import-helpers/order-imports': [
      'warn',
      {
        newlinesBetween: 'always',
        groups: [
          '/^react/',
          'module',
          tsConfigPathsImportHelpersGroup,
          ['parent', 'sibling', 'index'],
          '/.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$/'
        ],
        alphabetize: { order: 'asc', ignoreCase: true }
      }
    ],
    'react/prop-types': ['off'],
    'react/no-unescaped-entities': ['off'],
    'import/no-anonymous-default-export': ['off'],
    'react/display-name': ['off']
  }
}
