const runPrettier = 'prettier --write'
const runTests = 'cross-env CI=true yarn test --bail --findRelatedTests'

module.exports = {
  './*.+(js|jsx|ts|tsx)': [runPrettier, runTests],
  'src/**/*.+(js|jsx|ts|tsx)': [
    'eslint --fix-dry-run --ext .ts,.tsx,js,jsx src/**/*.+(ts|tsx|js|jsx)',
    runPrettier,
    runTests
  ]
}
