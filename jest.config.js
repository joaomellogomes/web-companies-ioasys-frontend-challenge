const { name } = require('./package.json')
const tsconfig = require('./tsconfig.base.json')
const moduleNameMapper = require('tsconfig-paths-jest')(tsconfig)

module.exports = {
  displayName: name,
  preset: 'ts-jest',
  testEnvironment: 'node',
  name,
  moduleFileExtensions: ['js', 'jsx', 'ts', 'tsx'],
  transform: {
    '^.+\\.(ts|tsx)?$': 'ts-jest',
    '^.+\\.(js|jsx)$': 'babel-jest',
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub'
  },
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/tsconfig.json',
      allowJs: true
    }
  },
  moduleNameMapper: {
    ...moduleNameMapper,
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      'jest-transform-stub'
  },
  setupFilesAfterEnv: ['<rootDir>/src/setupTests.ts']
}
