import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'

const App: React.FC = () => (
  <Router>
    <div className="App">Hello World</div>
  </Router>
)

export default App
