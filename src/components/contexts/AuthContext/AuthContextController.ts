import { useEffect, useState } from 'react'

export interface IAuthContext {
  signed: boolean
  loading: boolean
  // eslint-disable-next-line @typescript-eslint/ban-types
  user: object | null
  signIn(): Promise<void>
  signOut(): void
}

export const useAuthContextController = (): IAuthContext => {
  // eslint-disable-next-line @typescript-eslint/ban-types
  const [user, setUser] = useState<object | null>(null)
  const [loading, setLoading] = useState(true)

  const signIn = async () => {
    setUser({ name: 'Stub name ' })
    setLoading(false)

    return
  }

  const signOut = () => {
    return
  }

  useEffect(() => {
    function loadUserFromStorage() {
      const user = window.localStorage.getItem('user')

      if (user) setUser(JSON.parse(user))
    }

    loadUserFromStorage()
  }, [])

  return { signIn, signOut, signed: !!user, user, loading }
}
