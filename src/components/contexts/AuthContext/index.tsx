import React, { createContext, useContext } from 'react'

import { useAuthContextController, IAuthContext } from './AuthContextController'

const AuthContext = createContext<IAuthContext>({} as IAuthContext)

export const AuthProvider: React.FC = ({ children }) => (
  <AuthContext.Provider value={{ ...useAuthContextController() }}>{children}</AuthContext.Provider>
)

export const useAuth = () => useContext(AuthContext)
