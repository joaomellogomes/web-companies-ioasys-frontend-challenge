interface IInvestor {
  readonly id: number
  investor_name: string
  email: string
  city: string
  country: string
  balance: number
  photo: string | null
  portfolio: {
    enterprises_number: number
    enterprises: []
  }
  portfolio_value: number
  first_access: boolean
  super_angel: boolean
}

interface IUser {
  investor: IInvestor
  enterprise: null
}

export class User {
  public investor: IInvestor | undefined
  public enterprise: null

  constructor(userProps: IUser) {
    Object.assign(this, userProps)
  }
}
