import { User } from '@entities/User'
import { api } from '@network/api'
import to from 'await-to-js'

import { IAuthUserRepository } from './IAuthUserRepository'

export class AuthUserRepositoryImplementation implements IAuthUserRepository {
  async signWithEmailAndPassword(email: string, password: string): Promise<User | null> {
    const apiCall = () => api.post<User>('users/auth/sign_in', { data: { email, password } })

    const [error, response] = await to(apiCall())

    if (error) return null

    return response?.data ?? null
  }
}
