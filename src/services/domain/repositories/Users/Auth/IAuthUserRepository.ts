import { User } from '@services/domain/entities/User'

export interface IAuthUserRepository {
  signWithEmailAndPassword(email: string, password: string): Promise<User | null>
}
