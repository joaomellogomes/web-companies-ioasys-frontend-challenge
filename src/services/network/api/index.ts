import axios from 'axios'

const headers = new Headers()

headers.append('Accept', 'application/json')
headers.append('Content-Type', 'application/json')

export const api = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL ?? '',
  responseType: 'json',
  withCredentials: true,
  headers
})
